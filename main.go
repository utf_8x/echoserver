package main

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
)

var conf *Config
var confErr error

func main() {
	conf, confErr = LoadConfig("config.yaml")
	if confErr != nil {
		log.Fatalf("Could not load config: %s", confErr)
		return
	}

	http.HandleFunc("/", handleIndex)
	http.HandleFunc("/healthz", handleHealthz)
	http.HandleFunc("/echo", handleEcho)

	bind := fmt.Sprintf("%s:%d", conf.HTTP.BindAddress, conf.HTTP.BindPort)

	log.Infof("Starting HTTP server at %s", bind)
	log.Fatal(http.ListenAndServe(bind, nil))
}

func handleIndex(w http.ResponseWriter, req *http.Request) {
	type indexResponse struct {
		Service string `json:"service"`
	}

	body, err := json.Marshal(indexResponse{
		Service: "echoserver/1.0.0",
	})

	if err != nil {
		w.WriteHeader(500)
		log.Error(err)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(200)
	w.Write(body)
}

func handleHealthz(w http.ResponseWriter, req *http.Request) {
	log.Infof("[%s] HIT /healthz - FROM %s", req.Method, req.RemoteAddr)

	type healthzResponse struct {
		Status string `json:"status"`
	}

	j, err := json.Marshal(healthzResponse{Status: "ok"})
	if err != nil {
		w.WriteHeader(500)
		log.Error(err)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(200)
	w.Write(j)
}

func handleEcho(w http.ResponseWriter, req *http.Request) {
	log.Infof("[%s] HIT /echo - FROM %s", req.Method, req.RemoteAddr)

	if req.Method != "POST" {
		w.WriteHeader(405)
		log.Infof("Request denied with '405 method not allowed'")
		return
	}

	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		w.WriteHeader(500)
		log.Error(err)
		return
	}

	m := make(map[string]interface{})
	if err := json.Unmarshal(body, &m); err != nil {
		w.WriteHeader(400)
		log.Error(err)
		return
	}

	j, err := json.Marshal(m)
	if err != nil {
		w.WriteHeader(500)
		log.Error(err)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(200)
	w.Write(j)
}