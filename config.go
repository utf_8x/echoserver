package main

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type Config struct {
	HTTP struct {
		BindAddress string `yaml:"bindAddress"`
		BindPort uint `yaml:"bindPort"`
	} `yaml:"http"`
}

func LoadConfig(path string) (*Config, error) {
	configFile, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	var config Config
	if err := yaml.Unmarshal(configFile, &config); err != nil {
		return nil, err
	}
	return &config, nil
}
