FROM golang:1.16-alpine

WORKDIR /app

COPY . .

RUN go build -o echoserver

EXPOSE 8080/tcp

ENTRYPOINT [ "/app/echoserver" ]